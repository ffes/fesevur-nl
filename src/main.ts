import { enableProdMode, LOCALE_ID, importProvidersFrom } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { BrowserModule, bootstrapApplication } from '@angular/platform-browser';
import { provideAnimations } from '@angular/platform-browser/animations';
import localeNl from '@angular/common/locales/nl';
import localeNlExtra from '@angular/common/locales/extra/nl';

import { environment } from './environments/environment';
import { BlogService } from './app/blog.service';
import { AppRoutingModule } from './app/app-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AppComponent } from './app/app.component';

if (environment.production) {
	enableProdMode();
}

bootstrapApplication(AppComponent, {
	providers: [
		importProvidersFrom(
			AppRoutingModule,
			BrowserModule,
			NgxSpinnerModule,
		),
		BlogService,
		{ provide: LOCALE_ID, useValue: 'nl' },
		provideHttpClient(withInterceptorsFromDi()),
		provideAnimations()
	]
})
.catch(err => console.error(err));

registerLocaleData(localeNl, 'nl', localeNlExtra);
