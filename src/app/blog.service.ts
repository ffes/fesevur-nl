import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, Observer } from 'rxjs';

import { environment } from '../environments/environment';

@Injectable({
	providedIn: 'root'
})

// Docs of the WordPress REST API: https://developer.wordpress.org/rest-api/
export class BlogService {

	constructor(private http: HttpClient) { }

	getPosts() {
		const url = `${environment.apiRootUrlWordPress}/posts?per_page=25`;
		return this.http.get(url).pipe(catchError(this.errorHandler));
	}

	getPostsByTag(tag: number) {
		let url = `${environment.apiRootUrlWordPress}/posts?per_page=25`;
		if (tag !== 0)
			url += "&tags[terms]=" + tag;
		return this.http.get(url).pipe(catchError(this.errorHandler));
	}

	getPostById(id: any) {
		const url = `${environment.apiRootUrlWordPress}/posts/${id}`;
		return this.http.get(url).pipe(catchError(this.errorHandler));
	}

	getTags() {
		const url = `${environment.apiRootUrlWordPress}/tags`;
		return this.http.get(url).pipe(catchError(this.errorHandler));
	}

	errorHandler(error: HttpErrorResponse) {
		return new Observable((observer: Observer<any>) => {
			observer.error(error);
		});
	}
}
