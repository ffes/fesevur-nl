import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { PostComponent } from './post/post.component';
import { SinglePostComponent } from './single-post/single-post.component';

const routes: Routes = [
	{ path: '', component: HomeComponent },
	{ path: 'blog', component: PostComponent },
	{ path: 'posts', redirectTo: 'blog', pathMatch: 'full' },
	{ path: 'posts/:id', component: SinglePostComponent },
	{ path: '**', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
