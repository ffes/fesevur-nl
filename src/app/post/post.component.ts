import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgIf, NgFor, SlicePipe, DatePipe } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { RouterLink } from '@angular/router';

import { AppComponent } from '../app.component';
import { BlogService } from '../blog.service';
import { NgxSpinnerService, NgxSpinnerModule } from 'ngx-spinner';
import { WP_REST_API_Error, WP_REST_API_Posts, WP_REST_API_Tags } from 'wp-types';

@Component({
	selector: 'app-post',
	templateUrl: './post.component.html',
	styleUrls: ['./post.component.scss'],
	encapsulation: ViewEncapsulation.None,
	imports: [
		DatePipe,
		NgFor,
		NgIf,
		NgxSpinnerModule,
		RouterLink,
		SlicePipe,
	]
})
export class PostComponent implements OnInit {

	posts: WP_REST_API_Posts = [];
	tags: WP_REST_API_Tags = [];
	errorMessage: string = "";
	title: string = "Blog";

	constructor(private blogService: BlogService,
		private titleService: Title,
		private spinner: NgxSpinnerService) { }

	ngOnInit() {
		// Update de titel van de pagina
		this.titleService.setTitle(this.title + " - " + AppComponent.getTitle());

		this.getPosts();
		this.getTags();
	}

	onTag(id: any) {
		this.spinner.show();
		this.blogService.getPostsByTag(+id).subscribe({
			next: (data: WP_REST_API_Posts) => {
				this.posts = data;
				//console.log(data);
				this.spinner.hide();
			},
			error: (error: WP_REST_API_Error) => {
				this.errorMessage = error.message;
				//console.log(error);
				this.spinner.hide();
			}
		})
	}

	getPosts(): void {
		this.spinner.show();
		this.blogService.getPosts().subscribe({
			next: (data: WP_REST_API_Posts) => {
				this.posts = data;
				//console.log(data);
				this.spinner.hide();
			},
			error: (error: WP_REST_API_Error) => {
				this.errorMessage = error.message;
				//console.log(error);
				this.spinner.hide();
			}
		})
	}

	getTags(): void {
		this.blogService.getTags().subscribe({
			next: (data: WP_REST_API_Tags) => {
				this.tags = data;
			}
		})
	}
}
