import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AppComponent } from '../app.component';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

	constructor(
		private titleService: Title) { }

	ngOnInit(): void {
		// Update de titel van de pagina
		this.titleService.setTitle(AppComponent.getTitle());
	}
}
