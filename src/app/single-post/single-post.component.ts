import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { NgIf, DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { NgxSpinnerService, NgxSpinnerModule } from 'ngx-spinner';
import { WP_REST_API_Error, WP_REST_API_Post } from 'wp-types';
import { BlogService } from '../blog.service';
import { AppComponent } from '../app.component';

@Component({
	selector: 'app-single-post',
	templateUrl: './single-post.component.html',
	styleUrls: ['./single-post.component.scss'],
	encapsulation: ViewEncapsulation.None,
	imports: [
		DatePipe,
		NgIf,
		NgxSpinnerModule,
	]
})
export class SinglePostComponent implements OnInit {

	post!: WP_REST_API_Post;
	errorMessage: string = "";
	id!: string;
	minutes: number = 0;

	constructor(private blogService: BlogService,
		private route: ActivatedRoute,
		private titleService: Title,
		private spinner: NgxSpinnerService,) { }

	ngOnInit() {
		this.id = this.route.snapshot.paramMap.get('id') as string;
		this.getSinglePost();
	}

	private htmlDecode(input: string): string {
		var e = document.createElement('textarea');
		e.innerHTML = input;
		return e.value;
	}

	// Bepaal hoe lang je er (ongeveer) over doet om te lezen.
	// Gemiddeld leest men 200 woorden per minuut.
	private setMinutes(): void {
		const words = this.post?.content.rendered.replace(/<[^>]*>?/gm, '');
		const wordsCount = words.split(' ').length;
		this.minutes = Math.floor(wordsCount / 200) + 1;
	}

	getSinglePost(): void {

		this.spinner.show();
		this.blogService.getPostById(this.id).subscribe({
			next: (data: WP_REST_API_Post) => {
				this.post = data;

				// Update de titel van de pagina
				const appTitle: string = AppComponent.getTitle();
				const postTitle: string = this.htmlDecode(this.post.title.rendered);
				this.titleService.setTitle(postTitle + " - " + appTitle);

				this.setMinutes();

				// Verberg de spinner
				this.spinner.hide();
			},
			error: (error: WP_REST_API_Error) => {
				this.errorMessage = error.message;
				this.spinner.hide();
			}
		})
	}
}
