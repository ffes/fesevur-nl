# Website Familie Fesevur

Dit is de sourcecode van de website van de [familie Fesevur](https://www.fesevur.nl).

In basis een placeholder om je naar de juiste site te verwijzen.

Maar je vindt er ook onze vakantieblog.
Hiervoor wordt [WordPress](https://fesevur.wordpress.com) gebruikt als headless CMS voor de Angular frontend.
