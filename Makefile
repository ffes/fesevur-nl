error:
	@echo "No target given"

prepare:
	[ -d node_modules ] || npm install

serve: prepare
	npm start

clean:
	rm -rf dist/ .angular/

veryclean: clean
	rm -rf node_modules/ package-lock.json
